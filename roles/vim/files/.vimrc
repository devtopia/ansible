" 日本語文字コード自動判別と文字コード交換
" vimが内部で用いるエンコーディングを指定
set encoding=utf-8
scriptencoding utf-8

" 端末の出力に用いられるエンコーディングを指定
set termencoding=utf-8

" ファイルを開く時、適切なエンコーディングを自動的に判定
set fileencodings=utf-8,iso-2022-jp,euc-jp,sjis

" 想定される改行の種類を指定
set fileformats=unix,mac,dos

" ○や□の文字が崩れる問題を回避
" set ambiwidth=double

" Manage plugins
call plug#begin('~/.vim/plugged')
Plug 'flazz/vim-colorschemes'
Plug 'itchyny/dictionary.vim'
Plug 'vim-jp/vimdoc-ja'
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/fzf.vim'
Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'honza/vim-snippets'
Plug 'Shougo/vimproc.vim', { 'do': 'make' }
Plug 'Shougo/unite.vim'
Plug 'Shougo/neoyank.vim'
Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'
Plug 'Shougo/neocomplete'
Plug 'ervandew/supertab'
Plug 'jpalardy/vim-slime'
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
Plug 'cohama/agit.vim'
Plug 'szw/vim-tags'
Plug 'thinca/vim-ref'
Plug 'slim-template/vim-slim', { 'for': 'slim' }
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-rails', { 'for': ['ruby', 'eruby'] }
Plug 'tpope/vim-bundler', { 'for': ['ruby', 'eruby'] }
Plug 'vim-ruby/vim-ruby', { 'for': ['ruby', 'eruby'] }
Plug 'osyo-manga/vim-monster', { 'for': ['ruby', 'eruby'] }
Plug 'basyura/unite-rails', { 'for': ['ruby', 'eruby'] }
Plug 'thoughtbot/vim-rspec', { 'for': ['ruby', 'eruby'] }
Plug 'depuracao/vim-rdoc', { 'for': ['ruby', 'eruby'] }
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx']  }
Plug 'othree/yajs.vim', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'othree/es.next.syntax.vim', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'othree/javascript-libraries-syntax.vim', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'othree/html5.vim'
Plug 'maxmellon/vim-jsx-pretty', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'ternjs/tern_for_vim', { 'for': ['javascript', 'javascript.jsx'], 'do': 'npm install' }
Plug 'elzr/vim-json'
Plug 'kchmck/vim-coffee-script', { 'for': 'coffee' }
Plug 'JulesWang/css.vim'
Plug 'cakebaker/scss-syntax.vim'
Plug 'Lokaltog/vim-easymotion'
Plug 'thinca/vim-quickrun'
Plug 'joker1007/vim-markdown-quote-syntax'
Plug 'rcmdnk/vim-markdown'
Plug 'timcharper/textile.vim'
Plug 'tyru/open-browser.vim'
Plug 'kannokanno/previm'
Plug 'tsaleh/vim-tmux'
Plug 'jiangmiao/auto-pairs'
Plug 'tomtom/tcomment_vim'
Plug 'devtopia/sudo.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Plug 'edkolev/tmuxline.vim'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
Plug 'jwhitley/vim-matchit'
Plug 'vimtaku/hl_matchit.vim'
Plug 'majutsushi/tagbar'

" Add plugins to &runtimepath
call plug#end()

" Configure plugins
augroup MyVimrc
  autocmd!
augroup END

" https://github.com/itchyny/dictionary.vim
map <silent> ,d :Dictionary<CR>

" https://github.com/Shougo/neoyank.vim
let g:unite_enable_start_insert=1
let g:unite_source_history_yank_enable =1
let g:unite_source_file_mru_limit = 200
nmap <silent> ,uy :<C-u>Unite history/yank<CR>
nmap <silent> ,ub :<C-u>Unite buffer<CR>
nmap <silent> ,uf :<C-u>UniteWithBufferDir -buffer-name=files file<CR>
nmap <silent> ,ur :<C-u>Unite -buffer-name=register register<CR>
nmap <silent> ,uu :<C-u>Unite file_mru buffer<CR>

function! EnableJavascript()
  " Setup used libraries
  let g:used_javascript_libs = 'jquery,underscore,react,flux,jasmine,d3'
  let b:javascript_lib_use_jquery = 1
  let b:javascript_lib_use_underscore = 1
  let b:javascript_lib_use_react = 1
  let b:javascript_lib_use_flux = 1
  let b:javascript_lib_use_jasmine = 1
  let b:javascript_lib_use_d3 = 1
endfunction
autocmd MyVimrc FileType javascript,javascript.jsx call EnableJavascript()

" https://github.com/basyura/unite-rails
function! SetUniteRails()
  nmap <buffer><C-H><C-H><C-H>  :<C-U>Unite rails/view<CR>
  nmap <buffer><C-H><C-H>       :<C-U>Unite rails/model<CR>
  nmap <buffer><C-H>            :<C-U>Unite rails/controller<CR>
  nmap <buffer><C-H>c           :<C-U>Unite rails/config<CR>
  nmap <buffer><C-H>s           :<C-U>Unite rails/spec<CR>
  nmap <buffer><C-H>m           :<C-U>Unite rails/db -input=migrate<CR>
  nmap <buffer><C-H>l           :<C-U>Unite rails/lib<CR>
  nmap <buffer><expr><C-H>g     ':e '.b:rails_root.'/Gemfile<CR>'
  nmap <buffer><expr><C-H>r     ':e '.b:rails_root.'/config/routes.rb<CR>'
  nmap <buffer><expr><C-H>se    ':e '.b:rails_root.'/db/seeds.rb<CR>'
  nmap <buffer><C-H>ra          :<C-U>Unite rails/rake<CR>
  nmap <buffer><C-H>h           :<C-U>Unite rails/heroku<CR>
endfunction
autocmd MyVimrc User Rails call SetUniteRails()

" https://github.com/jpalardy/vim-slime
let g:slime_target = "tumx"
let g:slime_paste_file = "$HOME/.slime_paste"

" https://github.com/slim-template/vim-slim
autocmd MyVimrc BufNewFile,BufRead *.slim setlocal filetype=slim

" https://github.com/scrooloose/syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_enable_signs = 1
let g:syntastic_error_symbol = "✗"
let g:syntastic_warning_symbol = "⚠"
let g:syntastic_mode_map = {'mode': 'passive', 'active_filetypes': ['ruby', 'eruby', 'slim', 'javascript', 'javascript.jsx', 'coffee', 'scss']}
let g:syntastic_ruby_checkers = ['rubocop', 'mri']
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exec = 'eslint_d'
let g:syntastic_coffee_checkers = ['coffeelint']
let g:syntastic_scss_checkers= ['scss_lint']
autocmd MyVimrc FileType ruby,eruby if exists('b:rails_root') |
  \ let b:syntastic_ruby_rubocop_options = '--rails' | endif
function! ToggleErrors()
  if empty(filter(tabpagebuflist(), 'getbufvar(v:val, "&buftype") is# "quickfix"'))
    " No location/quickfix list shown, open syntastic error location panel
    Errors
  else
    lclose
  endif
endfunction
nnoremap <silent> ee :<C-u>call ToggleErrors()<CR>

" https://github.com/scrooloose/nerdtree
map <C-e> :NERDTreeToggle<CR>
let g:NERDTreeWinPos    = "left"
let g:NERDTreeDirArrows = 1
let g:NERDTreeMinimalUI = 1
let g:NERDTreeShowHidden = 1

" https://github.com/easymotion/vim-easymotion
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)

" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)

" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

" Gif config
map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)

let g:EasyMotion_startofline = 0 " keep cursor column when JK motion

" https://github.com/szw/vim-tags
function! SetVimTags()
  let g:vim_tags_auto_generate = 1
  let g:vim_tags_project_tags_command = "{CTAGS} -R --languages=+ruby,+coffee,-html,+javascript,-c,-c++ --exclude=vendor/bundle {OPTIONS} {DIRECTORY} 2>/dev/null"
  let g:vim_tags_gems_tags_command = "{CTAGS} -R --languages=+ruby,-coffee,-html,-javascript,-c,-c++ {OPTIONS} `bundle show --paths` 2>/dev/null"
  nmap <C-]> g<C-]>
  NeoCompleteTagMakeCache
endfunction
autocmd MyVimrc FileType ruby,eruby,javascript,javascript.jsx call SetVimTags()

" https://github.com/thinca/vim-quickrun
let g:quickrun_config = {
      \ '_': {
      \   'runner': 'vimproc',
      \   'runner/vimproc/updatetime': 100,
      \   'outputter': 'multi:buffer:quickfix',
      \   'outputter/buffer/split': '',
      \   'outputter/buffer/close_on_empty': 1
      \ }
      \ }

" Configure markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_frontmatter = 1
autocmd MyVimrc BufRead,BufNewFile *.{md,mdown,mkd,mkdn,mark*} set filetype=markdown
autocmd MyVimrc BufRead,BufNewFile *.textile set filetype=textile
map ,w :PrevimOpen<CR>

" Powerline系フォントを利用する
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#whitespace#mixed_indent_algo = 1
let g:airline_theme = 'papercolor'
" let g:airline_theme = 'luna'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" old vim-powerline symbols
" let g:airline_left_sep = '⮀'
" let g:airline_left_alt_sep = '⮁'
" let g:airline_right_sep = '⮂'
" let g:airline_right_alt_sep = '⮃'
" let g:airline_symbols.branch = '⭠'
" let g:airline_symbols.readonly = '⭤'
" let g:airline_symbols.linenr = '⭡'

" tmuxline
" let g:tmuxline_preset = {
"       \'a'    : '#S',
"       \'c'    : ['#(whoami)'],
"       \'win'  : ['#I', '#W'],
"       \'cwin' : ['#I', '#W', '#F'],
"       \'x'    : ['#{battery_icon}#{battery_percentage}', '#{cpu_icon}#{cpu_percentage}'],
"       \'y'    : ['%a', '%R', '#(ansiweather -l tokyo -w false -h false -p false -a false | cut -d " " -f7,8,9)'],
"       \'z'    : '#H',
"       \'options' : {'status-justify':'left'}}
" let g:tmuxline_theme = 'papercolor'

" https://github.com/nathanaelkane/vim-indent-guides
" vim立ち上げたときに、自動的にvim-indent-guidesをオンにする
let g:indent_guides_enable_on_vim_startup = 1
" ガイドをスタートするインデントの量
let g:indent_guides_start_level           = 2
" 自動カラーを無効にする
let g:indent_guides_auto_colors           = 0
" ハイライト色の変化の幅
let g:indent_guides_color_change_percent  = 30
" ガイドの幅
let g:indent_guides_guide_size            = 1

" Completion
" Enable omni completion.
autocmd MyVimrc FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd MyVimrc FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
" autocmd FileType javascript,javascript.jsx setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd MyVimrc FileType javascript,javascript.jsx setlocal omnifunc=tern#Complete
autocmd MyVimrc FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd MyVimrc FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
autocmd MyVimrc FileType ruby,eruby setlocal omnifunc=rubycomplete#Complete

" SuperTab
let g:SuperTabDefaultCompletionType = "<c-n>"
let g:SuperTabContextDefaultCompletionType = "<c-n>"
let g:SuperTabClosePreviewOnPopupClose = 1

" Note: This option must be set in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
let g:neocomplete#sources#omni#input_patterns.ruby = '[^. *\t]\.\w*\|\h\w*::'

" Configure snippet
" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif

" Enable snipMate compatibility feature.
let g:neosnippet#enable_snipmate_compatibility = 1
" Tell Neosnippet about the other snippets
let g:neosnippet#snippets_directory='~/.vim/snippets,~/.vim/plugged/vim-snippets/snippets'

" https://github.com/junegunn/vim-easy-align
" Start interactive EasyAlign in visual mode
vmap <CR> <Plug>(EasyAlign)

" matchit
let g:hl_matchit_enable_on_vim_startup = 1
let g:hl_matchit_hl_groupname = 'Title'
let g:hl_matchit_allow_ft = 'html\|vim\|ruby\|sh'

" https://github.com/tpope/vim-rails
function! SetVimRails()
  let g:rails_default_file='config/database.yml'
  let g:rails_level=4
  let g:rails_mappings=1
  let g:rails_modelines=0
  " let g:rails_some_option = 1
  " let g:rails_statusline = 1
  " let g:rails_subversion=0
  " let g:rails_syntax = 1
  " let g:rails_url='http://localhost:3000'
  " let g:rails_ctags_arguments='--languages=-javascript'
  " let g:rails_ctags_arguments = ''

  nmap <buffer><Space>r :R<CR>
  nmap <buffer><Space>a :A<CR>
  nmap <buffer><Space>m :Rmodel<Space>
  nmap <buffer><Space>c :Rcontroller<Space>
  nmap <buffer><Space>v :Rview<Space>
  nmap <buffer><Space>p :Rpreview<CR>
endfunction
autocmd MyVimrc User Rails call SetVimRails()

" https://github.com/thoughtbot/vim-rspec
function! SetVimRspec()
  map ,c :call RunCurrentSpecFile()<CR>
  map ,n :call RunNearestSpec()<CR>
  map ,l :call RunLastSpec()<CR>
  map ,a :call RunAllSpecs()<CR>
  let g:rspec_command = "Dispatch rspec {spec}"
  " iTerm instead of Terminal
  let g:rspec_runner = "os_x_iterm"
endfunction
autocmd MyVimrc FileType ruby call SetVimRspec()

" https://github.com/thinca/vim-ref
let g:ref_refe_cmd = 'refe' "refeコマンドのパス

" https://github.com/kchmck/vim-coffee-script
autocmd MyVimrc BufRead,BufNewFile *.coffee set filetype=coffee
map <silent> cf :CoffeeWatch vert<CR>

" https://github.com/cakebaker/scss-syntax.vim
autocmd MyVimrc BufRead,BufNewFile *.scss set filetype=scss.css

" https://github.com/majutsushi/tagbar
let g:tagbar_ctags_bin = 'ctags'
let g:tagbar_type_ruby = {
    \ 'kinds' : [
        \ 'm:modules',
        \ 'c:classes',
        \ 'd:describes',
        \ 'C:contexts',
        \ 'f:methods',
        \ 'F:singleton methods'
    \ ]
\ }
let g:tagbar_type_coffee = {
    \ 'ctagstype' : 'coffee',
    \ 'kinds'     : [
        \ 'c:classes',
        \ 'm:methods',
        \ 'f:functions',
        \ 'v:variables',
        \ 'f:fields',
    \ ]
\ }
let g:tagbar_type_css = {
\ 'ctagstype' : 'Css',
    \ 'kinds'     : [
        \ 'c:classes',
        \ 's:selectors',
        \ 'i:identities'
    \ ]
\ }
let g:tagbar_type_markdown = {
    \ 'ctagstype' : 'markdown',
    \ 'kinds' : [
        \ 'h:Heading_L1',
        \ 'i:Heading_L2',
        \ 'k:Heading_L3'
    \ ]
\ }
map <silent> tt :TagbarToggle<CR>

" 読み込んだプラグインも含め、ファイルタイプを検出、
" ファイルタイプ別プラグイン、インデントを有効化する。
filetype plugin indent on

" 256カラーを使う。
set t_Co=256
set term=screen-256color

" 選択した範囲のインデントサイズを連続変更
vnoremap < <gv
vnoremap > >gv

" 構文ハイライト有効
syntax on
set synmaxcol=200
set background=dark
" colorscheme molokai

" fzf
set rtp+=/usr/local/bin/fzf

" インクリメンタル検索を有効
set incsearch

" 検索のとき大小文字無視
set ignorecase

" 検索ハイライト有効
set hlsearch

" ex)
" foobar: 大小文字区分しない。
" FOOBAR: される。
" FooBar: される。
set smartcase

" ESCキー連打でさりげなく色を消す。
nmap <Esc><Esc> :nohlsearch<CR><Esc>

" 折り返しをしない。
set wrap
" markdown,textileの場合は行末で折り返す。
autocmd MyVimrc FileType markdown,textile set nowrap

" オートインデントを使用するため必要
set nopaste

" 行番号を表示する。
set number

" カーソルを左右に移動させる。
set whichwrap=b,s,h,l,[,],<,>,~

" Configure indent
" タブ入れを複数の空白入力に置き換える。
set expandtab

" 画面上でタグ文字が占める幅
set tabstop=2

" 自動インデントでずれる幅
set shiftwidth=2

" 連続した空白に対してタグキーやバックスペースキーでカーソルが動く幅
set softtabstop=2

" 改行の時に前の行のインデントを継続する。
set autoindent

" 改行の時に入力された行の末尾に合わせて次の行のインデントを増減する。
set smartindent

" mouse
" normal(n), visual(v), insert(i), command(c) = all(a)
set mouse=a

" Configure enable mouse drag
set ttymouse=xterm2

" backspace
" start: インサートモードの開始位置より前の文字列の削除を許可する。。
" indent: 自動インデントを行った場合、インデント部分の削除を許可する。
" eol: 改行文字の削除を許可する。
set backspace=start,indent,eol

" Configure vim's directory
set backup
set swapfile
set backupdir=~/.vim/backup
set directory=~/.vim/swap

" Configure vim's folding
" zc: close, zo: open, zC: close all, zO: open all
set foldmethod=syntax
set foldlevel=100
autocmd MyVimrc InsertEnter * if !exists('w:last_fdm')
  \| let w:last_fdm=&foldmethod
  \| setlocal foldmethod=manual
  \| endif

autocmd MyVimrc InsertLeave,WinLeave * if exists('w:last_fdm')
  \| let &l:foldmethod=w:last_fdm
  \| unlet w:last_fdm
  \| endif

" Reload vimrc file
noremap <Space>. :<C-u>edit $MYVIMRC<CR>
noremap <Space>s. :<C-u>source $MYVIMRC<CR>

" Make comment color to lightseagreen
hi Comment ctermfg=37

" Make color when visual mode used
hi Visual ctermfg=Black ctermbg=229

" Make colorscheme to dark color
autocmd MyVimrc VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=237
autocmd MyVimrc VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=235
hi colorcolumn ctermbg=235
" Make colorscheme to light color
" autocmd MyVimrc VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=255
" autocmd MyVimrc VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=254
" hi colorcolumn ctermbg=253

" Display vertical line at 80 columns
" execute "set colorcolumn=" . join(range(81, 81), ',')

" Remove all white space in a file
autocmd MyVimrc BufWritePre * :%s/\s\+$//ge

" Configure short command for buffer
nmap <silent>bp :bprevious<CR>
nmap <silent>bn :bnext<CR>
nmap <silent>bb :b#<CR>
nmap <silent>bf :bf<CR>
nmap <silent>bl :bl<CR>
nmap <silent>bm :bm<CR>
nmap <silent>bd :bdelete<CR>
