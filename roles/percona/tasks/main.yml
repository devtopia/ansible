- name: remove yum packages about mariadb
  yum:
    name: "{{ item }}"
    state: removed
  with_items:
    - mariadb-libs

- name: install MySQL-python if os is CentOS 7.x
  yum: name=MySQL-python state=present
  when:
    - ansible_distribution == "CentOS"
    - ansible_distribution_major_version|int == 7

- name: install MySQL-python3 if os is CentOS 8.x
  dnf: name=MySQL-python3 state=present
  when:
    - ansible_distribution == "CentOS"
    - ansible_distribution_major_version|int >= 8

- name: enable percona from firewall
  firewalld: port={{ item }} permanent=yes state=enabled
  with_items:
    - 3306/tcp
    - 4444/tcp
    - 4567/tcp
    - 4568/tcp

- name: reload firewalld
  service: name=firewalld state=reloaded

- name: add percona repository
  yum:
    name: https://repo.percona.com/yum/percona-release-latest.noarch.rpm
    state: present

- name: show enabled repository of percona
  shell: percona-release show
  register: percona_repository

- name: print enabled repository of percona
  ansible.builtin.debug: var=percona_repository.stdout

- name: enable only percona pxc-80
  shell: |
    percona-release disable all
    percona-release enable pxc-80 release
    percona-release enable tools release
    percona-release enable proxysql
  when: item not in percona_repository.stdout
  with_items:
    - pxc-80
    - tools
    - proxysql

- name: install the percona
  yum: name={{ item }} state=latest
  with_items: "{{ percona_packages }}"

- name: check if mysql log file size is 0 or not
  stat: path={{ mysql_log_file }}
  register: log_file
  changed_when: false

- name: enable skip-grant-tables mode
  blockinfile:
    dest: /etc/my.cnf
    content: |
      skip-grant-tables
  when: log_file.stat.size == 0

- name: start all nodes in percona xtradb cluster
  service: name=mysqld state=started enabled=yes
  when: log_file.stat.size == 0

# - name: get the temporary root password
#   shell: cat {{ mysql_log_file }} | grep 'temporary password'
#   register: temporary_password
#   changed_when: false
#   when: log_file.stat.size == 0

- name: change the root password
  command: mysql --user root --connect-expired-password --execute="FLUSH PRIVILEGES; ALTER USER 'root'@'localhost' IDENTIFIED BY '{{ mysql_user_password }}';"
  when: log_file.stat.size == 0

# - name: change root password from temporary password
#   mysql_user:
#     login_host: 'localhost'
#     login_user: 'root'
#     login_password: ''
#     name: 'root'
#     password: "{{ mysql_user_password }}"
#     state: present
#   when: log_file.stat.size == 0

# - name: change the root password
#   community.mysql.mysql_query:
#     login_user: root
#     login_password: ""
#     query: FLUSH PRIVILEGES; ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY %s;
#     positional_args:
#       - "{{ mysql_user_password }}"
#   when: log_file.stat.size == 0

# - name: create a database user to use instead of the root user
#   community.mysql.mysql_user:
#     login_user: "{{ mysql_user_name }}"
#     login_password: "{{ mysql_user_password }}"
#     name: "{{ mysql_admin_user.name }}"
#     host: "{{ mysql_admin_user.host }}"
#     password: "{{ mysql_admin_user.password }}"
#     priv: '*.*:ALL,GRANT'
#     state: present
#   when:
#     - log_file.stat.size == 0
#     - wsrep_node_name == 'db1'

# - name: create a sstuser to use the percona xtradb cluster
#   community.mysql.mysql_user:
#     login_user: "{{ mysql_user_name }}"
#     login_password: "{{ mysql_user_password }}"
#     name: sstuser
#     host: localhost
#     password: "{{ sstuser_password }}"
#     priv: '*.*:RELOAD,LOCK TABLES,PROCESS,REPLICATION CLIENT'
#     state: present
#   when:
#     - log_file.stat.size == 0
#     - wsrep_node_name == 'db1'

- name: stop all nodes in percona xtradb cluster
  service: name=mysqld state=stopped

- name: disable skip-grant-tables mode
  replace:
    dest: /etc/my.cnf
    regexp: "^skip-grant-tables"
    replace: "# skip-grant-tables"

# - name: create the wsrep.conf in /etc/percona-xtradb-cluster.conf.d
#   template:
#     dest: /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
#     src: wsrep.cnf.j2
#     mode: 0644
#     backup: yes

- name: create the my.cnf in /etc
  template:
    dest: /etc/my.cnf
    src: my.cnf.j2
    mode: 0644
    backup: yes

- name: bootstrapping the first node
  service: name=mysql@bootstrap.service state=started
  when: wsrep_node_name == 'db1'

- name: pause for 5 seconds
  pause: seconds=5

- name: add nodes to percona xtradb cluster
  service: name=mysqld state=started
  when: wsrep_node_name != 'db1'
