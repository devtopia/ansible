# Playbookの説明

詳細説明は[Wiki](https://gitlab.com/devtopia/ansible/wikis/home)をご参照ください。

## dev.yml（開発環境構築）

このプレイブックを実行すると以下の開発環境が構築されます。
開発作業を行うサーバーを構築する目的で作ったので、複数のユーザーの開発環境を作られます。

### ruby

* rbenvを使ってrubyのバージョン管理を行う。
* 最新バージョンのrubyをglobalで設定する。

### node

* nvmを使ってnodeのバージョン管理を行う。
* 最新バージョンのnodeをglobalで設定する。
* globalで設定したnodeでyarnをglobalで設置する。

### python

* 最新バージョンをソースコンパイルして設置する。
* nvim（nvim用のpluginをvimで使う為に設置する。）
* ansible（ansibleの最新バージョンを設置する。）
* ansible-lint（ansibleのlintingを支援）

### vim

* luaを設置する。
* vim-plugでpluginを管理を行う。
* ruby, node, pythonの開発に対応
* ruby, node, pythonのコード補完（coc.nvimというlanguage clientを利用）
* ruby, node, pythonのlinting（aleを利用してコードのlinting、fixを行う。）

### その他

* nginx
* mysql client
* powerline
* tmux

### 使い方

```sh
ansible-playbook dev.yml

# ruby環境のみ構築する場合
ansible-playbook dev.yml -t ruby

# node環境のみ構築する場合
ansible-playbook dev.yml -t node

# python環境のみ構築する場合
ansible-playbook dev.yml -t python

# mysql-clientのみ設置したい場合
ansible-playbook dev.yml -t mysql
```

## local.yml（Macの初期設定）

Macの初期設定を自動化するためのPlaybookです。
個人用のプログラミング環境を構築します。（dotfiles）
App Storeのアプリやhomebrewのパッケージを設置します。
ricty fontを設置します。
vagrantやそのpluginを設置します。

設定ファイルをご自分用に変更して使ってください。

```yaml
git_repositories:
  - {repo: "https://gitlab.com/devtopia/dotfiles.git", dest: ~/dotfiles, update: 'no'}
  - {repo: "https://github.com/tmux-plugins/tpm", dest: ~/dotfiles/.tmux/plugins/tpm, force: 'yes'}
  - {repo: "https://github.com/mbadolato/iTerm2-Color-Schemes.git", dest: ~/dotfiles/iTerm2-Color-Schemes, force: 'yes'}

symlinks:
  - {src: ~/dotfiles/.ansiweatherrc, dest: ~/.ansiweatherrc}
  - {src: ~/dotfiles/.eslintrc, dest: ~/.eslintrc}
  - {src: ~/dotfiles/.gemrc, dest: ~/.gemrc}
  - {src: ~/dotfiles/.tern-project, dest: ~/.tern-project}
  - {src: ~/dotfiles/.tmux, dest: ~/.tmux}
  - {src: ~/dotfiles/.tmux.conf, dest: ~/.tmux.conf}
  - {src: ~/dotfiles/.vim, dest: ~/.vim}
  - {src: ~/dotfiles/.vimrc, dest: ~/.vimrc}
  - {src: ~/dotfiles/.zsh, dest: ~/.zsh}
  - {src: ~/dotfiles/.zshenv, dest: ~/.zshenv}
  - {src: ~/dotfiles/.zshrc, dest: ~/.zshrc}
  - {src: ~/dotfiles/Vagrantfile, dest: ~/Vagrantfile}

homebrew_taps:
  - homebrew/services
  - homebrew/cask-fonts
  - sanemat/font
  - caskroom/cask
  - beeftornado/rmtree

homebrew_cask_packages:
  - android-file-transfer
  - calibre
  - dash
  - diskmaker-x
  - google-chrome
  - google-backup-and-sync
  - handbrake
  - ifunbox
  - iina
  - iterm2
  - java
  - keycastr
  - licecap
  - mysqlworkbench
  - skype
  - transmission
  - vmware-fusion
  - vagrant
  - vagrant-manager
  - vagrant-vmware-utility
  - xquartz

homebrew_packages:
  - {name: ansible}
  - {name: ansiweather}
  - {name: ctags}
  - {name: emojify}
  - {name: fontforge}
  - {name: fzf}
  - {name: jq}
  - {name: lua}
  - {name: luajit}
  - {name: mysql}
  - {name: mas}
  - {name: mongodb}
  - {name: nvm}
  - {name: overmind}
  - {name: peco}
  - {name: postgresql}
  - {name: python3}
  - {name: rbenv}
  - {name: rbenv-gemset}
  - {name: readline}
  - {name: reattach-to-user-namespace}
  - {name: redis}
  - {name: ricty, install_options: with-powerline}
  - {name: ripgrep}
  - {name: ruby-build}
  - {name: sqlite}
  - {name: "http://git.io/sshpass.rb"}
  - {name: tmux}
  - {name: tree}
  - {name: vim}
  - {name: wget}
  - {name: zplug}
  - {name: zsh}

homebrew_services:
  - mysql
  - mongodb
  - postgresql

apps:
  - {id: 688211836, name: EasyRes}
  - {id: 1206246482, name: "EdgeView 2"}
  - {id: 441258766, name: Magnet}
  - {id: 715768417, name: "Microsoft Remote Desktop"}
  - {id: 539883307, name: LINE}
  - {id: 869223134, name: KakaoTalk}
  - {id: 409183694, name: Keynote}
  - {id: 409203825, name: Numbers}
  - {id: 409201541, name: Pages}
  - {id: 407963104, name: Pixelmator}
  - {id: 434808346, name: SimpleMind}
  - {id: 408981434, name: iMovie}
  - {id: 682658836, name: GarageBand}
  - {id: 568020055, name: Scapple}

vagrant_plugins:
  - sahara
  - vagrant-cachier
  - vagrant-vbguest
  - vagrant-vmware-desktop

vmware_license_file: "~/Documents/vmware/license.lic"
```

### 使い方

一部アプリの設置はシステム環境設定のところでセキュリティを許可する必要があります。
Playbookの実行中に同意を求めるポップアップが表示された場合、同意してください。
同意しないとPlaybookの実行が失敗してしまいます。

```sh
# システムのファイルに弄る場合があるので、sudoを付けて実行する。
sudo ansible-playbook local.yml

# homebrewパッケージのみ設置したい場合
sudo ansible-playbook local.yml -t homebrew
```

## web.yml

web applicationの駆動環境を構築します。
主にstagingとlive環境の構築するために作りました。

## db.yml

mysql replication構成でデータベースを構築します。

## git.yml

最新のgitを設置します。

## gitlab.yml

gitlabサーバーを構築します。

## runner.yml

gitlab ciサーバーを構築します。

## redmine.yml

redmineサーバーを構築します。

## gembox.yml

privateなgemサーバーを構築します。

## その他、rolesの説明

### mariadb

mariadbのデータベースを構築します。

### postgresql

postgresqlのクライアントを設置します。

### mysql-client

mysqlのクライアントを設置します。

## トラブル整理

### ansibleのgem moduleを使う時の注意点

ruby環境でgemを設置する時、shell moduleを使っていたのをgem moduleに変更したの時の問題

* https://qiita.com/itiut@github/items/3e1aaa4f2b5d95efb319

user_installというオプションをfalseにしないと.rbenv以下ではなく.gmeの以下にgemが設置される。
設置したgemを見つけられなくてcommand not foundが発生するので注意！

### pipで設置したライブラリのコマンドを見つけない。（command not found）
